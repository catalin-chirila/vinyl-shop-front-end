export enum ColorPalette {
    '#EBE3D9',
    '#E0CDAF',
    '#C2BC74',
    '#99908b',
    '#a3a1a5',
    '#B8B8B8'
}
