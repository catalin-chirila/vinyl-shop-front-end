export enum UserRoles {
    ROLE_USER = 'ROLE_USER',
    ROLE_STORE_MANAGER = 'ROLE_STORE_MANAGER'
}
