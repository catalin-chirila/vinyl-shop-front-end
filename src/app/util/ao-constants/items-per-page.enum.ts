export enum ItemsPerPage {
    SHOPPING_CART = 6,
    ORDERS = 6,
    PROFILE = 6,
    MANAGE_VINYLS = 6,
    MANAGE_ORDERS = 6,
    CUSTOMERS = 6,
    HOME = 9
}
