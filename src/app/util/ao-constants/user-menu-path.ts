import { MenuItem } from 'src/app/model/menu-item';

export class UserMenuPath {
    static readonly SHOPPING_CART: MenuItem = { name: 'Shopping Cart', path: '/profile/cart' };
    static readonly PROFILE: MenuItem = { name: 'Profile', path: '/profile/details' };
    static readonly MANAGE_VINYLS: MenuItem = { name: 'Manage Vinyls', path: '/profile/manage-vinyls' };
    static readonly CUSTOMERS: MenuItem = { name: 'Customers', path: '/profile/customers' };

    private constructor() { }
}
