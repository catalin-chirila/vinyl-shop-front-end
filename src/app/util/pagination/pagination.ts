import { Subject, Observable } from 'rxjs';
import { PageIndexes } from 'src/app/model/page-indexes';

export class Page {
    startIndex: number;
    endIndex: number;
    totalItemsAmount: number;
    itemsAmountPerPage: number;

    private pageIndexesSubject = new Subject<PageIndexes>();

    /**
     * @param itemsAmountPerPage the amount of items to be displayed on a single page
     * @param totalItemsAmount the total amount of items that needs to be displayed in separated pages
     */
    constructor(itemsAmountPerPage: number, totalItemsAmount: number) {
        this.startIndex = 0;
        this.endIndex = itemsAmountPerPage - 1;
        this.itemsAmountPerPage = itemsAmountPerPage;
        this.totalItemsAmount = totalItemsAmount;
        this.updateCurrentPage();
    }

    nextPage(): void {
        if (this.endIndex + 1 < this.totalItemsAmount) {
            this.startIndex = this.endIndex + 1;
            this.endIndex = this.startIndex + this.itemsAmountPerPage - 1;
        }

        this.updateCurrentPage();
    }

    previousPage(): void {
        if (this.startIndex + 1 !== 1) {
            this.startIndex -= this.itemsAmountPerPage;
            this.endIndex = this.startIndex + this.itemsAmountPerPage - 1;
        }

        this.updateCurrentPage();
    }

    updateCurrentPage(): void {
        const pageIndex = {
            startIndex: 0,
            endIndex: 0
        };

        if (this.totalItemsAmount > this.endIndex) {
            pageIndex.startIndex = this.startIndex;
            pageIndex.endIndex = this.endIndex + 1;
        } else if (this.totalItemsAmount > this.startIndex) {
            pageIndex.startIndex = this.startIndex;
            pageIndex.endIndex = this.totalItemsAmount;
            this.endIndex = this.totalItemsAmount - 1;
        } else if (this.startIndex === this.endIndex && this.startIndex !== 0) {
            this.previousPage();
            pageIndex.startIndex = this.startIndex;
            pageIndex.endIndex = this.endIndex + 1;
        }

        this.pageIndexesSubject.next(pageIndex);
    }

    get pageIndexesValue(): Observable<PageIndexes> {
        return this.pageIndexesSubject.asObservable();
    }
}
