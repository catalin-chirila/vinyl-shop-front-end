import { Component } from '@angular/core';

@Component({
  selector: 'app-carousel-banner',
  templateUrl: './carousel-banner.component.html',
  styleUrls: ['./carousel-banner.component.scss']
})
export class CarouselBannerComponent {
  index = 0;
  speed = 8000;
  infinite = true;
  direction = 'right';
  directionToggle = true;
  autoplay = true;

  avatars = [
    {
      url:
        'https://images.unsplash.com/photo-1558114073-e77f284dfb76?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjF9&auto=format&fit=crop&w=1350&q=80',
      title: 'BANNER TITLE1',
      secondaryTitle: 'Artist Name'
    },
    {
      url:
        'https://images.unsplash.com/photo-1526815456940-2c11653292a2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80',
      title: 'BANNER TITLE2',
      secondaryTitle: 'Artist Name'
    },
    {
      url:
        'https://images.unsplash.com/photo-1519677584237-752f8853252e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80',
      title: 'BANNER TITLE3',
      secondaryTitle: 'Artist Name'
    }
  ];
  constructor() { }

  toggleDirection($event) {
    this.direction = this.directionToggle ? 'right' : 'left';
  }
}
