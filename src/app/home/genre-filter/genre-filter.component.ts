import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-genre-filter',
  templateUrl: './genre-filter.component.html',
  styleUrls: ['./genre-filter.component.scss']
})
export class GenreFilterComponent implements OnInit {
  genres = [];

  constructor() { }

  ngOnInit() {
    this.genres.push({name: 'Rock', selected: false});
    this.genres.push({name: 'Jazz', selected: false});
    this.genres.push({name: 'Disco', selected: false});
    this.genres.push({name: 'Pop', selected: false});
    this.genres.push({name: 'Soundtrack', selected: false});
  }

}
