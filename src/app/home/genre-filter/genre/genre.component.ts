import { Component, OnInit, Input } from '@angular/core';
import { FilterService } from '../../../service/filter.service';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.scss']
})
export class GenreComponent implements OnInit {
  @Input() genre: string;
  isSelected = false;

  constructor(private filterService: FilterService) {}

  ngOnInit() {
  }

  toggleSelection() {
    this.isSelected = this.isSelected ? false : true;

    if (this.isSelected) {
      this.filterService.addTag(this.genre);
    } else {
      this.filterService.removeTag(this.genre);
    }
  }

}
