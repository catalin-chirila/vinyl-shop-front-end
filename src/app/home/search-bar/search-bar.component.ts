import { Component, OnInit } from '@angular/core';
import { FilterService } from '../../service/filter.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent {
  constructor(private filterService: FilterService) {}

  search(searchText: string): void {
    this.filterService.updateSearchText(searchText);
  }
}
