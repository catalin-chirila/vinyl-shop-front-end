import { Component, Input } from '@angular/core';
import { VinylService } from 'src/app/service/vinyl.service';
import { MatSnackBar } from '@angular/material';
import { ShoppingCart } from 'src/app/model/shopping-cart';
import { ShoppingCartService } from 'src/app/service/account-overview/shopping-cart.service';
import { Vinyl } from 'src/app/model/vinyl';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
  selector: 'app-vinyl',
  templateUrl: './vinyl.component.html',
  styleUrls: ['./vinyl.component.scss']
})
export class VinylComponent {
  @Input() id: number;
  @Input() name: string;
  @Input() price: number;
  @Input() author: string;

  constructor(private vinylService: VinylService,
              private shoppingCartService: ShoppingCartService,
              private snackBar: MatSnackBar,
              private authService: AuthService) { }

  processVinyl(): void {
    if (this.authService.currentUserValue) {
      this.getVinylQuantityFromCart(this.id).then(quantity =>
        this.addVinylToCart(quantity + 1)
      ).catch(error =>
        this.openSnackBar('Vinyl could not be added to cart. Please try again.', 'close')
      );
    } else {
      this.openSnackBar('Please log in to add a vinyl to cart.', 'close')
    }
  }

  addVinylToCart(quantity: number): void {
    this.vinylService.addVinylToCart(this.id, quantity).subscribe(
      success => {
        this.shoppingCartService.updateCartItemsAmount();
        this.openSnackBar(`Vinyl ${this.name} added to cart.`, 'close');
      },
      error => this.openSnackBar('Vinyl could not be added to cart. Please try again.', 'close'))
  }

  private getVinylQuantityFromCart(vinylId: number): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      this.shoppingCartService.getCartDetails().subscribe(
        (cart: ShoppingCart) => {
          resolve(this.getVinylQuantityFromCartItems(vinylId, cart.items));
        },
        (err) => {
          reject();
        }
      );
    });
  }

  private getVinylQuantityFromCartItems(vinylId: number, items: Vinyl[]): number {
    if (items === null) {
      return 0;
    }

    const vinyl = items.find(v => v.id === vinylId);

    if (vinyl === null || vinyl === undefined) {
      return 0;
    }

    return vinyl.quantity;
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
      panelClass: ['snackbar']
    });
  }
}
