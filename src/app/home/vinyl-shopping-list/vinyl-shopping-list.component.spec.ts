import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VinylShoppingListComponent } from './vinyl-shopping-list.component';

describe('VinylShoppingListComponent', () => {
  let component: VinylShoppingListComponent;
  let fixture: ComponentFixture<VinylShoppingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VinylShoppingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VinylShoppingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
