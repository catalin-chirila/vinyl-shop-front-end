import { Component, OnInit } from '@angular/core';
import { FilterService } from 'src/app/service/filter.service';
import { VinylService } from 'src/app/service/vinyl.service';
import { Vinyl } from 'src/app/model/vinyl';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-vinyl-shopping-list',
  templateUrl: './vinyl-shopping-list.component.html',
  styleUrls: ['./vinyl-shopping-list.component.scss']
})
export class VinylShoppingListComponent implements OnInit {
  vinyls: Vinyl[] = [];
  searchText = '';
  tags: string[] = [];

  constructor(private filterService: FilterService,
              private vinylService: VinylService,
              private snackBar: MatSnackBar) {
    this.vinylService.getAllVinyls().subscribe(vinylList => {
      this.vinyls = vinylList.vinyls;
    }, error => {
      this.snackBar.open('Error getting vinyls. Please refresh the page.', 'close', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    });
  }

  ngOnInit(): void {
    this.filterService.searchTextValue.subscribe(searchText => (this.searchText = searchText));
    this.filterService.tagsValue.subscribe(tags => (this.tags = tags));
  }
}
