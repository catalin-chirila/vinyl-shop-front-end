import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { DialogService } from 'src/app/service/dialog.service';
import { MatSnackBar } from '@angular/material';
import { AuthService } from 'src/app/service/auth/auth.service';
import { ShoppingCartService } from 'src/app/service/account-overview/shopping-cart.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  errorMessage = '';
  deleteAccountForm: FormGroup;
  submitted = false;
  email: string;
  firstName: string;
  lastName: string;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router,
    private dialogService: DialogService,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private cartService: ShoppingCartService
  ) {}

  ngOnInit() {
    this.deleteAccountForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.maxLength(255)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]]
    });

    this.email = this.authService.currentUserValue.email;
    this.firstName = this.authService.currentUserValue.firstName;
    this.lastName = this.authService.currentUserValue.lastName;
  }

  deleteAccount() {
    this.dialogService.openYesNoDialog(`Are you sure you want to delete your account?`).subscribe(choice => {
      if (choice) {
        this.submitted = true;

        if (this.deleteAccountForm.invalid) {
          return;
        }

        this.userService.deleteAccount(this.deleteAccountForm.value).then(() => {
          this.authService.logOut();
          this.router.navigate(['/home']);
          this.snackBar.open('The account was deleted successfully!', 'close', {
            duration: 5000,
            panelClass: ['snackbar']
          });
        }).catch(() => {
          this.errorMessage = 'The account could not be deleted! Please verify the email/password and try again.';
        });
      }
    });
  }

  get f() {
    return this.deleteAccountForm.controls;
  }

}
