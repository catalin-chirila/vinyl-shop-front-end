import { Component, OnInit, Inject } from '@angular/core';
import { ColorPalette } from '../../../util/ao-constants/color-palette.enum';
import { Page } from 'src/app/util/pagination/pagination';
import { Customer } from 'src/app/model/customer';
import { MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { OrderService } from 'src/app/service/order.service';
import { Orders } from 'src/app/model/orders';
import { HttpErrorResponse } from '@angular/common/http';
import { ItemsPerPage } from '../../../util/ao-constants/items-per-page.enum';
import { Order } from 'src/app/model/order';
import { OrderStatus } from 'src/app/model/order-status';

@Component({
  selector: 'app-customer-orders',
  templateUrl: './customer-orders.component.html',
  styleUrls: ['./customer-orders.component.scss']
})
export class CustomerOrdersComponent implements OnInit {
  orders = [];
  ordersView = [];
  ordersToUpdate: OrderStatus[] = [];
  colorPalette = ColorPalette;
  statuses = ['PLACED', 'SENT', 'DELIVERED', 'CANCELED'];
  page: Page;

  constructor(@Inject(MAT_DIALOG_DATA) public customer: Customer, private orderService: OrderService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.initOrdersData();
  }

  initOrdersData(): void {
    this.orderService.getCustomerOrders(this.customer.id).subscribe((orderList: Orders) => {
      this.ordersToUpdate = [];
      this.sortOrders(orderList.orders);
      this.initPagination();
    }, (err: HttpErrorResponse) => {
      this.snackBar.open('Error getting vinyls. Please refresh the page.', 'close', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    });
  }

  private sortOrders(orders: Order[]): void {
    this.orders = orders.sort((a, b) => {
      const value = a.orderDate.localeCompare(b.orderDate);
      if (value < 0) {
        return 1;
      } else if (value > 0) {
        return -1;
      }

      return value;
    });
  }

  private initPagination(): void {
    this.ordersView = this.orders.slice(0, ItemsPerPage.MANAGE_ORDERS);
    this.page = new Page(ItemsPerPage.MANAGE_VINYLS, this.orders.length);
    this.page.pageIndexesValue.subscribe(pageIndexes =>
      this.ordersView = this.orders.slice(pageIndexes.startIndex, pageIndexes.endIndex)
    );
  }

  changeOrderStatus(newStatus: string, originalOrder: Order): void {
    const orderFound = this.ordersToUpdate.find(o => {
      return o.orderId === originalOrder.id;
    });

    if (orderFound) {
      if (newStatus === originalOrder.status) {
        this.ordersToUpdate = this.ordersToUpdate.filter(o => o.orderId !== originalOrder.id);
      } else {
        orderFound.status = newStatus;
      }
    } else if (newStatus !== originalOrder.status) {
      this.ordersToUpdate.push({ orderId: originalOrder.id, status: newStatus });
    }
  }

  updateStatuses(): void {
    if (this.ordersToUpdate.length === 0) {
      this.snackBar.open('There aren\'t any changes to save.', 'close', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    }

    this.orderService.updateOrderStatus(this.ordersToUpdate).subscribe(success => {
      this.initOrdersData();
      this.snackBar.open('The statuses were updated with success.', 'close', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    }, err => {
      this.snackBar.open('There was an error updating the statuses. Please try again.', 'close', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    });
  }

  nextPage(): void {
    this.page.nextPage();
  }

  previousPage(): void {
    this.page.previousPage();
  }
}
