import { Component, OnInit } from '@angular/core';
import { ColorPalette } from '../../util/ao-constants/color-palette.enum';
import { Vinyl } from 'src/app/model/vinyl';
import { UserService } from 'src/app/service/user.service';
import { Customers } from 'src/app/model/customers';
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from 'src/app/util/pagination/pagination';
import { ItemsPerPage } from '../../util/ao-constants/items-per-page.enum';
import { DialogService } from 'src/app/service/dialog.service';
import { Customer } from 'src/app/model/customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  customers: Customer[] = [];
  customersView: Customer[] = [];
  colorPalette = ColorPalette;
  page: Page;

  constructor(private userService: UserService, private dialogService: DialogService) {}

  ngOnInit(): void {
    this.userService.getAllCustomers().subscribe(
      (customerList: Customers) => {
        this.customers = customerList.customers;
        this.initPagination();
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      }
    );
  }

  private initPagination(): void {
    this.customersView = this.customers.slice(0, ItemsPerPage.CUSTOMERS);
    this.page = new Page(ItemsPerPage.CUSTOMERS, this.customers.length);
    this.page.pageIndexesValue.subscribe(pageIndexes => {
      this.customersView = this.customers.slice(pageIndexes.startIndex, pageIndexes.endIndex);
    });
  }

  nextPage() {
    this.page.nextPage();
  }

  previousPage() {
    this.page.previousPage();
  }

  getTotalPrice(vinyls: Vinyl[]): string {
    let total = 0;

    for (const vinyl of vinyls) {
      total += vinyl.quantity * vinyl.price;
    }

    return Number(total).toFixed(2);
  }

  openCustomerOrders(customer: Customer): void {
    this.dialogService.openCustomerOrdersDialog(customer);
  }

}
