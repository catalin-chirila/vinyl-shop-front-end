import { Component, OnInit, Input } from '@angular/core';
import { MenuService } from 'src/app/service/menu.service';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {
  @Input() name: string;
  @Input() path: string;
  isSelected: boolean;

  constructor(private menuService: MenuService) {}

  ngOnInit() {
    this.menuService.currentMenuItemValue.subscribe(currentMenuItem => {
      this.isSelected = this.name === currentMenuItem ? true : false;
    });

    this.isSelected = this.name === this.menuService.initialMenuItem ? true : false;
  }

  toggleSelection() {
    this.menuService.setCurrentMenuItem({ name: this.name, path: this.path });
  }
}
