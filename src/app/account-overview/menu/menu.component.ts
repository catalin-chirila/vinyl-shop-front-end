import { Component, OnInit } from '@angular/core';
import { UserMenuPath } from '../../util/ao-constants/user-menu-path';
import { AuthService } from 'src/app/service/auth/auth.service';
import { UserRoles } from '../../util/ao-constants/user-roles.enum';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  userMenuPath = UserMenuPath;
  isCurrentUserStoreManager: boolean;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    const currentUser = this.authService.currentUserValue;
    this.isCurrentUserStoreManager = currentUser.roles.includes(UserRoles.ROLE_STORE_MANAGER);
  }
}
