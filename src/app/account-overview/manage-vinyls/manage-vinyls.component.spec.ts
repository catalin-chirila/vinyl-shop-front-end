import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageVinylsComponent } from './manage-vinyls.component';

describe('ManageVinylsComponent', () => {
  let component: ManageVinylsComponent;
  let fixture: ComponentFixture<ManageVinylsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageVinylsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageVinylsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
