import { Component, OnInit } from '@angular/core';
import { ColorPalette } from '../../util/ao-constants/color-palette.enum';
import { Vinyl } from 'src/app/model/vinyl';
import { VinylService } from 'src/app/service/vinyl.service';
import { Vinyls } from 'src/app/model/vinyls';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { ItemsPerPage } from '../../util/ao-constants/items-per-page.enum';
import { Page } from 'src/app/util/pagination/pagination';
import { DialogService } from 'src/app/service/dialog.service';

@Component({
  selector: 'app-manage-vinyls',
  templateUrl: './manage-vinyls.component.html',
  styleUrls: ['./manage-vinyls.component.scss']
})
export class ManageVinylsComponent implements OnInit {
  vinyls: Vinyl[] = [];
  vinylsView: Vinyl[] = [];
  colorPalette = ColorPalette;
  page: Page;

  constructor(private vinylService: VinylService, private snackBar: MatSnackBar, private dialogService: DialogService) { }

  ngOnInit(): void {
    this.vinylService.getAllVinyls().subscribe(
      (vinylList: Vinyls) => {
        this.vinyls = vinylList.vinyls;
        this.initPagination();
      },
      (err: HttpErrorResponse) => {
        this.snackBar.open('Error getting vinyls. Please refresh the page.', 'close', {
          duration: 5000,
          panelClass: ['snackbar']
        });
      }
    );
  }

  private initPagination(): void {
    this.vinylsView = this.vinyls.slice(0, ItemsPerPage.MANAGE_VINYLS);
    this.page = new Page(ItemsPerPage.MANAGE_VINYLS, this.vinyls.length);
    this.page.pageIndexesValue.subscribe(pageIndexes =>
      this.vinylsView = this.vinyls.slice(pageIndexes.startIndex, pageIndexes.endIndex)
    );
  }

  subtractQuantity(vinyl: Vinyl): void {
    if (vinyl.quantity - 1 > 0) {
      vinyl.quantity--;
      vinyl.totalPrice = vinyl.price * vinyl.quantity;
    }
  }

  incrementQuantity(vinyl: Vinyl): void {
    if (vinyl.quantity + 1 < 100) {
      vinyl.quantity++;
      vinyl.totalPrice = vinyl.price * vinyl.quantity;
    }
  }

  nextPage() {
    this.page.nextPage();
  }

  previousPage() {
    this.page.previousPage();
  }

  editVinyl(vinyl: Vinyl): void {
    this.dialogService.openEditVinylDialog(vinyl);
  }

  createVinyl(): void {
    this.dialogService.openCreateVinylDialog();
  }

  deleteVinyl(vinyl: Vinyl): void {
    this.dialogService.openYesNoDialog(`Are you sure you want to delete ${vinyl.name} permanently?`).subscribe(choice => {
      if (choice) {
        this.vinylService.deleteVinyl(vinyl.id).subscribe(success => {
          this.vinyls = this.vinyls.filter(v => v !== vinyl);
          this.page.totalItemsAmount = this.vinyls.length;
          this.page.updateCurrentPage();
          this.snackBar.open(`The vinyl ${vinyl.name} was deleted with success.`, 'close', {
            duration: 5000,
            panelClass: ['snackbar']
          });
        }, (err: HttpErrorResponse) => {
         this.processDeleteVinylError(err);
        });
      }
    });
  }

  processDeleteVinylError(err: HttpErrorResponse) {
    if (err.status === 403) {
      this.snackBar.open(`The vinyl could not be deleted because it is present in a purchase.`, 'close', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    } else {
      this.snackBar.open(`The vinyl could not be deleted. Please try again.`, 'close', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    }
  }
}
