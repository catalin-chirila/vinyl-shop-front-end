import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { Vinyl } from 'src/app/model/vinyl';
import { VinylService } from 'src/app/service/vinyl.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-vinyl',
  templateUrl: './create-vinyl.component.html',
  styleUrls: ['./create-vinyl.component.scss']
})
export class CreateVinylComponent implements OnInit {

  errorMessage = '';
  createVinylForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public vinyl: Vinyl,
    private dialogRef: MatDialogRef<CreateVinylComponent>,
    private router: Router,
    private snackBar: MatSnackBar,
    private vinylService: VinylService
  ) { }

  ngOnInit() {
    this.createVinylForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
      author: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
      price: ['', [Validators.required, Validators.min(0)]],
      genre: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(40)]],
      releaseYear: ['', [Validators.required, Validators.min(0)]],
      stock: ['', [Validators.required, Validators.min(0)]],
    });
  }

  get f() {
    return this.createVinylForm.controls;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
      panelClass: ['snackbar']
    });
  }

  createVinyl(): void {
    this.submitted = true;

    if (this.createVinylForm.invalid) {
      return;
    }

    this.vinylService.createVinyl(this.createVinylForm.value).subscribe(success => {
      this.dialogRef.close();

      this.router.navigate(['/profile/cart']).then(() => {
        this.router.navigate(['/profile/manage-vinyls']);
      });

      this.openSnackBar('The vinyl was created with success!', 'close');
    }, err => {
      this.openSnackBar('There was an error while creating the vinyl, please try again.', 'close');
    });
  }

}
