import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { Vinyl } from 'src/app/model/vinyl';
import { VinylService } from 'src/app/service/vinyl.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-vinyl',
  templateUrl: './edit-vinyl.component.html',
  styleUrls: ['./edit-vinyl.component.scss']
})
export class EditVinylComponent implements OnInit {

  errorMessage = '';
  editVinylForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public vinyl: Vinyl,
    private dialogRef: MatDialogRef<EditVinylComponent>,
    private router: Router,
    private snackBar: MatSnackBar,
    private vinylService: VinylService
  ) { }

  ngOnInit() {
    this.editVinylForm = this.formBuilder.group({
      name: [this.vinyl.name, [Validators.maxLength(100)]],
      author: [this.vinyl.author, [Validators.maxLength(100)]],
      price: [this.vinyl.price, [Validators.min(0)]],
      genre: [this.vinyl.genre, [Validators.maxLength(40)]],
      releaseYear: [this.vinyl.releaseYear, [Validators.min(0)]],
      stock: [this.vinyl.stock, [Validators.min(0)]],
    });
  }

  get f() {
    return this.editVinylForm.controls;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
      panelClass: ['snackbar']
    });
  }

  editVinyl(): void {
    this.submitted = true;

    if (this.editVinylForm.invalid) {
      return;
    }

    this.vinylService.updateVinyl(this.vinyl, this.editVinylForm.value).subscribe(success => {
      this.dialogRef.close();

      this.router.navigate(['/profile/cart']).then(() => {
        this.router.navigate(['/profile/manage-vinyls']);
      });

      this.openSnackBar('The vinyl was updated with success!', 'close');
    }, err => {
      this.openSnackBar('There was an error while updating the vinyl, please try again.', 'close');
    });
  }
}
