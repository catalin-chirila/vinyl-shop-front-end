import { Component, OnInit } from '@angular/core';
import { ColorPalette } from '../../util/ao-constants/color-palette.enum';
import { ShoppingCartService } from 'src/app/service/account-overview/shopping-cart.service';
import { Vinyl } from 'src/app/model/vinyl';
import { ShoppingCart } from 'src/app/model/shopping-cart';
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from 'src/app/util/pagination/pagination';
import { ItemsPerPage } from '../../util/ao-constants/items-per-page.enum';
import { MatSnackBar } from '@angular/material';
import { DialogService } from 'src/app/service/dialog.service';
import { UpdateQuantity } from 'src/app/model/update-quantity';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  cart: ShoppingCart;
  isCartEmpty = true;
  vinylsView: Vinyl[];
  page: Page;
  vinylsToBeUpdated: UpdateQuantity[] = [];
  colorPalette = ColorPalette;

  constructor(
    private shoppingCartService: ShoppingCartService,
    private dialogService: DialogService,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.shoppingCartService.getCartDetails().subscribe(
      (cart: ShoppingCart) => {
        if (cart.itemsAmount === 0) {
          this.isCartEmpty = true;
        } else {
          this.processCartData(cart);
          this.initPagination();
        }
      },
      (err: HttpErrorResponse) => {
        this.isCartEmpty = true;
        console.log(err);
      }
    );
  }

  private processCartData(cart: ShoppingCart): void {
    this.isCartEmpty = false;
    this.cart = cart;
    this.cart.items.sort((a, b) => a.name.localeCompare(b.name));
    this.cart.items.forEach(v => v.totalPrice = v.price * v.quantity);
  }

  private initPagination(): void {
    this.vinylsView = this.cart.items.slice(0, ItemsPerPage.SHOPPING_CART);
    this.page = new Page(ItemsPerPage.SHOPPING_CART, this.cart.items.length);
    this.page.pageIndexesValue.subscribe(pageIndexes => {
      this.vinylsView = this.cart.items.slice(pageIndexes.startIndex, pageIndexes.endIndex);
    });
  }

  subtractQuantity(vinyl: Vinyl): void {
    if (vinyl.quantity - 1 > 0) {
      vinyl.quantity--;
      vinyl.totalPrice = vinyl.price * vinyl.quantity;
      this.vinylsToBeUpdated.push({
        itemId: vinyl.id,
        newQuantity: vinyl.quantity
      });
    }
  }

  incrementQuantity(vinyl: Vinyl): void {
    if (vinyl.quantity + 1 < 100) {
      vinyl.quantity++;
      vinyl.totalPrice = vinyl.price * vinyl.quantity;
      this.vinylsToBeUpdated.push({
        itemId: vinyl.id,
        newQuantity: vinyl.quantity
      });
    }
  }

  removeVinyl(vinyl: Vinyl): void {
    this.dialogService.openYesNoDialog(`Are you sure you want to delete ${vinyl.name} from cart?`).subscribe(choice => {
      if (choice) {
        this.shoppingCartService.removeItem(vinyl.id).then(() => {
          this.cart.items = this.cart.items.filter(v => v !== vinyl);
          this.isCartEmpty = this.cart.items.length === 0 ? true : this.isCartEmpty;
          this.page.totalItemsAmount = this.cart.items.length;
          this.page.updateCurrentPage();
          this.openSnackBar('Item deleted successfully', 'close');
        }).catch(() => {
          this.openSnackBar('The item could not be deleted, please try again.', 'close');
        });
      }
    });
  }

  nextPage(): void {
    this.page.nextPage();
  }

  previousPage(): void {
    this.page.previousPage();
  }

  saveChanges(): void {
    if (this.vinylsToBeUpdated.length !== 0) {
      this.shoppingCartService.updateQuantity(this.vinylsToBeUpdated).subscribe(success => {
        this.openSnackBar('Changes saved!', 'close');
      }, err => {
        this.openSnackBar('The changes could not be saved, please try again.', 'close');
        console.log(err);
      });
    } else {
      this.openSnackBar('There aren\'t any changes to save.', 'close');
    }
  }

  placeOrder(): void {
    const total = this.cart.items.map(i => i.price).reduce((a, b) => a + b, 0);
    this.dialogService.openYesNoDialog(`Are you sure you want to place the order?`).subscribe(choice => {
      if (choice) {
        this.shoppingCartService.placeOrder().then(() => {
          this.vinylsView = [];
          this.isCartEmpty = true;
          this.openSnackBar('Order placed!', 'close');
        }).catch(() => {
          this.openSnackBar('The order could not be placed, please try again.', 'close');
        });
      }
    });
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
      panelClass: ['snackbar']
    });
  }
}
