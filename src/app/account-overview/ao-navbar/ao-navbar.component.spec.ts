import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AoNavbarComponent } from './ao-navbar.component';

describe('AoNavbarComponent', () => {
  let component: AoNavbarComponent;
  let fixture: ComponentFixture<AoNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AoNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AoNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
