import { Component, OnInit } from '@angular/core';
import { ColorPalette } from '../../util/ao-constants/color-palette.enum';
import { Vinyl } from 'src/app/model/vinyl';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent {
  orders = [];
  ordersView = [];
  colorPalette = ColorPalette;
  pageStartIndex = 0;
  pageEndIndex = 5;

  constructor() {
    for (let i = 0; i < 14; i++) {
      this.orders.push({
        items: [{
          name: 'Vinyl Name',
          author: 'Author Name',
          quantity: 1,
          price: 9.99
        },
        {
          name: 'Vinyl Name',
          author: 'Author Name',
          quantity: 5,
          price: 9.99
        }],
        creationDate: Date.now(),
        status: 'In Progress'
      });
    }

    this.ordersView = [...this.orders];
    this.updatePaging();
  }

  nextPage() {
    if (this.pageEndIndex + 1 < this.orders.length) {
      this.pageStartIndex = this.pageEndIndex + 1;
      this.pageEndIndex = this.pageStartIndex + 5;
      this.updatePaging();
    }
  }

  previousPage() {
    if (this.pageStartIndex + 1 !== 1) {
      this.pageStartIndex -= 6;
      this.pageEndIndex = this.pageStartIndex + 5;
      this.updatePaging();
    }
  }

  updatePaging() {
    if (this.orders.length > this.pageEndIndex) {
      this.ordersView = this.orders.slice(this.pageStartIndex, this.pageEndIndex + 1);
    } else if (this.orders.length > this.pageStartIndex) {
      this.ordersView = this.orders.slice(this.pageStartIndex, this.orders.length);
      this.pageEndIndex = this.orders.length - 1;
    }
  }

  totalPrice(vinyls: Vinyl[]): string {
    let total = 0;

    for (const vinyl of vinyls) {
      total += vinyl.quantity * vinyl.price;
    }

    return Number(total).toFixed(2);
  }

}
