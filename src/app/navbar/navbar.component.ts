import { Component, OnInit } from '@angular/core';
import { DialogService } from '../service/dialog.service';
import { AuthService } from '../service/auth/auth.service';
import { Router } from '@angular/router';
import { MenuService } from '../service/menu.service';
import { ShoppingCartService } from '../service/account-overview/shopping-cart.service';
import { ShoppingCart } from '../model/shopping-cart';
import { UserMenuPath } from '../util/ao-constants/user-menu-path';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  email = '';
  cartItemsAmount = 0;

  constructor(private dialogService: DialogService, private authService: AuthService, private menuService: MenuService,
              private router: Router, private cartService: ShoppingCartService) {

    this.email = authService.currentUserValue ? authService.currentUserValue.email.split('@')[0] : '';
  }

  ngOnInit(): void {
    this.authService.loggedUserEmail$.subscribe(email => {
      this.email = email === '' ? '' : email.split('@')[0];
      this.cartService.updateCartItemsAmount();
    });

    if (this.authService.currentUserValue === null) {
      this.cartItemsAmount = 0;
    } else {
      this.cartService.getCartDetails().subscribe((cart: ShoppingCart) => {
        this.cartItemsAmount = cart.items !== null ? cart.items.length : 0;
      });
    }

    this.cartService.cartItemsAmountValue.subscribe(itemsAmount => this.cartItemsAmount = itemsAmount);
  }

  openRegisterDialog(): void {
    this.dialogService.openRegisterDialog();
  }

  openLoginDialog(): void {
    this.dialogService.openLoginDialog();
  }

  logOut(): void {
    this.authService.logOut();
  }

  goToShoppingCart(): void {
    this.menuService.setCurrentMenuItem(UserMenuPath.SHOPPING_CART);
    this.router.navigate(['/profile/cart']);
  }

  goToProfile(): void {
    this.menuService.setCurrentMenuItem(UserMenuPath.PROFILE);
    this.router.navigate(['/profile/details']);
  }
}
