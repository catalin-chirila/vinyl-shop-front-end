import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { GenreFilterComponent } from './home/genre-filter/genre-filter.component';
import { GenreComponent } from './home/genre-filter/genre/genre.component';
import { CarouselBannerComponent } from './home/carousel-banner/carousel-banner.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchBarComponent } from './home/search-bar/search-bar.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { TagPipe } from './pipe/tag.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './register/register.component';
import { MatDialogModule, MatButtonModule, MatRippleModule } from '@angular/material';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AccountOverviewComponent } from './account-overview/account-overview.component';
import { MenuItemComponent } from './account-overview/menu/menu-item/menu-item.component';
import { MenuComponent } from './account-overview/menu/menu.component';
import { AoNavbarComponent } from './account-overview/ao-navbar/ao-navbar.component';
import { ShoppingCartComponent } from './account-overview/shopping-cart/shopping-cart.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { VinylComponent } from './home/vinyl-shopping-list/vinyl/vinyl.component';
import { VinylShoppingListComponent } from './home/vinyl-shopping-list/vinyl-shopping-list.component';
import { OrdersComponent } from './account-overview/orders/orders.component';
import { ProfileComponent } from './account-overview/profile/profile.component';
import { CustomersComponent } from './account-overview/customers/customers.component';
import { ManageVinylsComponent } from './account-overview/manage-vinyls/manage-vinyls.component';
import { YesNoDialogComponent } from './util/yes-no-dialog/yes-no-dialog.component';
import { JwtInterceptor } from './security/jwt-interceptor';
import { EditVinylComponent } from './account-overview/manage-vinyls/edit-vinyl/edit-vinyl.component';
import { CreateVinylComponent } from './account-overview/manage-vinyls/create-vinyl/create-vinyl.component';
import { CustomerOrdersComponent } from './account-overview/customers/customer-orders/customer-orders.component';
import { AuthGuardService } from './service/auth/auth-guard.service';
import { RoleGuardService } from './service/auth/role-guard.service';
import { UserRoles } from './util/ao-constants/user-roles.enum';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    VinylComponent,
    GenreFilterComponent,
    GenreComponent,
    VinylShoppingListComponent,
    CarouselBannerComponent,
    SearchBarComponent,
    TagPipe,
    RegisterComponent,
    LoginComponent,
    AccountOverviewComponent,
    MenuComponent,
    MenuItemComponent,
    AoNavbarComponent,
    ShoppingCartComponent,
    HomeComponent,
    OrdersComponent,
    ProfileComponent,
    CustomersComponent,
    ManageVinylsComponent,
    YesNoDialogComponent,
    EditVinylComponent,
    CreateVinylComponent,
    CustomerOrdersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxHmCarouselModule,
    FormsModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatTooltipModule,
    MatSnackBarModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatRippleModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent },
      {
        path: 'profile', component: AccountOverviewComponent, children: [
          { path: '', component: ShoppingCartComponent },
          { path: 'cart', component: ShoppingCartComponent },
          { path: 'details', component: ProfileComponent },
          {
            path: 'manage-vinyls', component: ManageVinylsComponent,
            canActivate: [RoleGuardService],
            data: { role: UserRoles.ROLE_STORE_MANAGER }
          },
          {
            path: 'customers', component: CustomersComponent,
            canActivate: [RoleGuardService],
            data: { role: UserRoles.ROLE_STORE_MANAGER }
          }
        ], canActivate: [AuthGuardService]
      },

      { path: '', component: HomeComponent },
      { path: '**', component: HomeComponent }
    ])
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [RegisterComponent, LoginComponent, YesNoDialogComponent, EditVinylComponent, CreateVinylComponent,
    CustomerOrdersComponent]
})
export class AppModule { }
