import { Pipe, PipeTransform } from '@angular/core';
import { Vinyl } from '../model/vinyl';

@Pipe({
  name: 'tagFilter',
  pure: false
})
export class TagPipe implements PipeTransform {
  transform(vinyls: Vinyl[], tags: string[]) {
    if (!tags.length) {
      return vinyls;
    }

    tags = tags.map(tag => tag.toLowerCase());

    return vinyls.filter(vinyl => tags.includes(vinyl.genre.toLowerCase()));
  }

}
