import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RegisterComponent } from '../register/register.component';
import { LoginComponent } from '../login/login.component';
import { YesNoDialogComponent } from '../util/yes-no-dialog/yes-no-dialog.component';
import { Observable } from 'rxjs';
import { EditVinylComponent } from '../account-overview/manage-vinyls/edit-vinyl/edit-vinyl.component';
import { Vinyl } from '../model/vinyl';
import { CreateVinylComponent } from '../account-overview/manage-vinyls/create-vinyl/create-vinyl.component';
import { CustomerOrdersComponent } from '../account-overview/customers/customer-orders/customer-orders.component';
import { Customer } from '../model/customer';

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  constructor(private dialog: MatDialog) { }

  openRegisterDialog(): void {
    this.dialog.open(RegisterComponent, {
      width: '320px',
      disableClose: false
    });
  }

  openLoginDialog(): void {
    this.dialog.open(LoginComponent, {
      width: '320px',
      disableClose: false
    });
  }

  openYesNoDialog(msg: string): Observable<boolean> {
    const yesNoDialog = this.dialog.open(YesNoDialogComponent, {
      data: { message: msg },
      disableClose: false
    });

    return yesNoDialog.afterClosed();
  }

  openEditVinylDialog(vinyl: Vinyl): void {
    this.dialog.open(EditVinylComponent, {
      data: vinyl,
      width: '320px',
      disableClose: false
    });
  }

  openCreateVinylDialog(): void {
    this.dialog.open(CreateVinylComponent, {
      disableClose: false
    });
  }

  openCustomerOrdersDialog(customer: Customer): void {
    this.dialog.open(CustomerOrdersComponent, {
      data: customer,
      width: '840px',
      disableClose: false
    });
  }
}
