import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Orders } from '../model/orders';
import { environment } from 'src/environments/environment';
import { OrderStatus } from '../model/order-status';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) {}

  getCustomerOrders(customerId: number): Observable<Orders> {
    return this.http.get<Orders>(`${environment.API_URL}/users/${customerId}/orders`);
  }

  updateOrderStatus(orderStatuses: OrderStatus[]): Observable<any> {
    const requests = [];

    orderStatuses.forEach(
      os => requests.push(this.http.post(`${environment.API_URL}/orders/${os.orderId}`, { status: os.status }))
    );

    return forkJoin(requests);
  }
}
