import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  private searchTextSubject = new Subject<string>();
  private tagsSubject = new Subject<string[]>();
  tags: string[];

  constructor() {
    this.tags = [];
  }

  updateSearchText(searchText: string): void {
    this.searchTextSubject.next(searchText);
  }

  addTag(tag: string): void {
    this.tags.push(tag);
    this.tagsSubject.next(this.tags);
  }

  removeTag(tag: string): void {
    this.tags = this.tags.filter(t => t !== tag);
    this.tagsSubject.next(this.tags);
  }

  get searchTextValue(): Observable<string> {
    return this.searchTextSubject.asObservable();
  }

  get tagsValue(): Observable<string[]> {
    return this.tagsSubject.asObservable();
  }
}
