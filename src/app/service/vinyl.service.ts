import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Vinyls } from '../model/vinyls';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Vinyl } from '../model/vinyl';

@Injectable({
  providedIn: 'root'
})
export class VinylService {

  constructor(private http: HttpClient) { }

  getAllVinyls(): Observable<Vinyls> {
    return this.http.get<Vinyls>(`${environment.API_URL}/vinyls`);
  }

  addVinylToCart(vinylId: number, vinylQuantity: number): Observable<any> {
    return this.http.post(`${environment.API_URL}/vinyls/${vinylId}/cart`, { quantity: vinylQuantity });
  }

  updateVinyl(original: Vinyl, updated: Vinyl): Observable<any> {
    const updatedVinyl = {
      name: updated.name !== '' ? updated.name : original.name,
      author: updated.author !== '' ? updated.author : original.author,
      price: updated.price !== null ? updated.price : original.price,
      genre: updated.genre !== '' ? updated.genre : original.genre,
      releaseYear: updated.releaseYear !== null ? updated.releaseYear : original.releaseYear,
      stock: updated.stock !== null ? updated.stock : original.stock
    };

    return this.http.put(`${environment.API_URL}/vinyls/${original.id}`, updatedVinyl);
  }

  createVinyl(vinyl: Vinyl): Observable<any> {
    return this.http.post(`${environment.API_URL}/vinyls`, vinyl);
  }

  deleteVinyl(vinylId: number): Observable<any> {
    return this.http.delete(`${environment.API_URL}/vinyls/${vinylId}`);
  }
}
