import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { CurrentUser } from 'src/app/model/current-user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserEmailSubject = new Subject<string>();
  private currentUserSubject: BehaviorSubject<CurrentUser>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<CurrentUser>(JSON.parse(localStorage.getItem('currentUser')));
  }

  login(payload: any): Promise<string> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'response' as 'response'
    };

    return new Promise((resolve, reject) => {
      this.http.post(`${environment.API_URL}/users/login`, payload, httpOptions).subscribe(
        (resp: HttpResponse<any>) => {
          const jwt = resp.headers.get('Authorization');

          this.getCurrentUser(jwt).subscribe(
            (currentUser: CurrentUser) => {
              currentUser.token = jwt;
              localStorage.setItem('currentUser', JSON.stringify(currentUser));
              this.currentUserSubject.next(currentUser);
              this.currentUserEmailSubject.next(currentUser.email);
              resolve('Success');
            },
            (err: HttpErrorResponse) => {
              reject(err.error.message);
            }
          );

        },
        (err: HttpErrorResponse) => {
          if (err.status === 401) {
            reject('Wrong email or password');
          } else {
            reject(err.error.message);
          }
        }
      );
    });
  }

  register(payload: any): Observable<HttpResponse<any>> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'response' as 'response'
    };

    return this.http.post(`${environment.API_URL}/users`, payload, httpOptions);
  }

  getCurrentUser(jwt: string): Observable<any> {
    return this.http.get<CurrentUser>(`${environment.API_URL}/users/current_info`, {
      headers: { Authorization: jwt }
    });
  }

  logOut(): void {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.currentUserEmailSubject.next('');
  }

  get loggedUserEmail$(): Observable<string> {
    return this.currentUserEmailSubject.asObservable();
  }

  get currentUserValue(): CurrentUser {
    return this.currentUserSubject.value;
  }

  isAuthenticated(): boolean {
    return this.currentUserValue !== null && this.currentUserValue !== undefined;
  }
}
