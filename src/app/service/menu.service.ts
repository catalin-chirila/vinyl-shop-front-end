import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { UserMenuPath } from '../util/ao-constants/user-menu-path';
import { MenuItem } from '../model/menu-item';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private currentMenuItemSubject = new Subject<string>();
  initialMenuItem = UserMenuPath.SHOPPING_CART.name;

  constructor(private router: Router) {
    if (this.router.url.includes('/profile')) {
      this.setCurrentMenuItem(UserMenuPath.SHOPPING_CART);
    }
  }

  setCurrentMenuItem(currentMenuItem: MenuItem) {
    this.initialMenuItem = currentMenuItem.name;
    this.currentMenuItemSubject.next(currentMenuItem.name);
    this.router.navigate([currentMenuItem.path]);
  }

  get currentMenuItemValue(): Observable<string> {
    return this.currentMenuItemSubject.asObservable();
  }
}
