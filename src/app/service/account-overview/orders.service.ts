import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Orders } from 'src/app/model/orders';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private http: HttpClient, private authService: AuthService) {}

  getAllOrders(userId: number): Observable<Orders> {
    return this.http.get<Orders>(`${environment.API_URL}/users/${userId}/orders`);
  }
}
