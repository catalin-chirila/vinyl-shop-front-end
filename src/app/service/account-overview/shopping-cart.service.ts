import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShoppingCart } from 'src/app/model/shopping-cart';
import { Observable, Subject } from 'rxjs';
import { UpdateQuantity } from 'src/app/model/update-quantity';
import { forkJoin } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  private cartItemsAmountSubject = new Subject<number>();

  constructor(private http: HttpClient, private authService: AuthService) { }

  getCartDetails(): Observable<ShoppingCart> {
    return this.http.get<ShoppingCart>(`${environment.API_URL}/users/${this.authService.currentUserValue.id}/cart`);
  }

  removeItem(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${environment.API_URL}/users/${this.authService.currentUserValue.id}/cart/${id}`).subscribe(success => {
        this.updateCartItemsAmount();
        resolve();
      }, (error) => {
        reject();
      });
    });
  }

  updateQuantity(items: UpdateQuantity[]): Observable<any> {
    const requests = [];

    items.forEach(
      i => requests.push(this.http.post(`${environment.API_URL}/vinyls/${i.itemId}/cart`, { quantity: i.newQuantity }))
    );

    return forkJoin(requests);
  }

  placeOrder(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${environment.API_URL}/${this.authService.currentUserValue.id}/orders`, null).subscribe(success => {
        this.updateCartItemsAmount();
        resolve();
      }, error => {
        reject();
      });
    });

  }

  updateCartItemsAmount(): void {
    if (!this.authService.currentUserValue) {
      this.cartItemsAmountSubject.next(0);
    } else {
      this.getCartDetails().subscribe((cart: ShoppingCart) => {
        const itemsAmount = cart.items !== null ? cart.items.length : 0;
        this.cartItemsAmountSubject.next(itemsAmount);
      });
    }
  }

  get cartItemsAmountValue(): Observable<number> {
    return this.cartItemsAmountSubject.asObservable();
  }
}
