import { Injectable } from '@angular/core';
import { Customers } from '../model/customers';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeleteAccount } from '../model/delete-account';
import { AuthService } from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  getAllCustomers(): Observable<Customers> {
    return this.http.get<Customers>(`${environment.API_URL}/users`);
  }

  deleteAccount(deleteAccount: DeleteAccount): Promise<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: deleteAccount
    };

    return new Promise((resolve, reject) => {
      this.http.delete(`${environment.API_URL}/users/${this.authService.currentUserValue.id}`, options)
        .subscribe(success => resolve(), err => reject());
    });
  }
}
