export interface Order {
    id: number;
    cost: number;
    orderDate: string;
    status: string;
}
