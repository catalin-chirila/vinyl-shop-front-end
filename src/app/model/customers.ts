import { Customer } from './customer';

export interface Customers {
    customers: Customer[];
}
