import { Vinyl } from './vinyl';

export interface Vinyls {
    vinyls: Vinyl[];
}
