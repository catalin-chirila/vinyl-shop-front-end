export interface Vinyl {
    id: number;
    name: string;
    price: number;
    quantity: number;
    author: string;
    genre: string;
    stock: number;
    releaseYear: Date;
    totalPrice: number;
}
