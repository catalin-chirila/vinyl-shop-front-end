export interface UpdateQuantity {
    itemId: number;
    newQuantity: number;
}
