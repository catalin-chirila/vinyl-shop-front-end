export interface PageIndexes {
    startIndex: number;
    endIndex: number;
}
