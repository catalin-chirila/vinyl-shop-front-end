import { Vinyl } from './vinyl';

export interface ShoppingCart {
    itemsAmount: number;
    totalCost: number;
    items: Vinyl[];
}
