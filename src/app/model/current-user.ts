export interface CurrentUser {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
    token?: string;
    roles: string[];
}
